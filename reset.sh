#!/bin/sh

echo "Removing lists..."
rm -rf data/lists/*

source scripts/create-folders.sh

echo "Restoring default config..."
cp webservice/config.json data/config/config.json

source scripts/get-ip.sh

echo "Restarting services..."
docker-compose restart

