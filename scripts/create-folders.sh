#!/bin/sh

echo "Creating data directories..."
mkdir -p data/lists &> /dev/null
mkdir -p data/logs &> /dev/null
mkdir -p data/certs &> /dev/null
mkdir -p data/config &> /dev/null