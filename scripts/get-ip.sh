#!/bin/sh


echo "Getting private IP address..."

if grep -q -v '^#' .env
then
    export $(grep -v '^#' .env);
else
    echo "Nothing set in .env";
fi

if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    # Linux
    ip=$(ip route get 8.8.8.8 | grep -oP 'src \K[^ ]+')
    export DNS_SERVER_IP_4==${HOST_IPV4:=$ip};
elif [[ "$OSTYPE" == "darwin"* ]]; then
    # Mac OSX
    ip=$(ifconfig en0 | awk '$1 == "inet" {print $2}')
    export DNS_SERVER_IP_4=${HOST_IPV4:=$ip};
fi

echo "Using ${DNS_SERVER_IP_4} as IPv4 address for DNS server..."
