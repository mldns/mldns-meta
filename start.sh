#!/bin/sh

source scripts/check-requirements.sh
source scripts/get-ip.sh

echo "Building Docker containers..."
docker-compose build

source scripts/create-folders.sh

if ! [ -f "data/config/config.json" ]; then
    echo "Setting default config"
    cp webservice/config.json data/config/config.json
fi

echo "Starting Docker containers..."
docker-compose up -d